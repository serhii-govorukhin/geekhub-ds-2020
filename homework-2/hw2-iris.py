#!/usr/bin/env python
# coding: utf-8

# In[43]:


import numpy as np


# In[44]:


np.__version__


# ### Read data

# In[45]:


# Read data
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
iris = np.genfromtxt(url, delimiter=',', dtype='object')
names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')


# ### 1 Extract ‘species’

# In[46]:


data = np.delete(iris, obj=4, axis=1)
print("Shape: ", data.shape)
print("Dimension: ", data.ndim)
print(data[:10])


# ### 2 Преобразовать первые 4 колонки в 2D массив

# In[47]:


matrix = data.astype(np.float)
print(matrix[:10])


# ### 3 Посчитать mean, median, standard deviation по 1-й колонке

# In[48]:


sepallength = matrix[:,:1]
print("mean: ", sepallength.mean())
print("median: ", np.median(sepallength))
print("standard deviation: ", sepallength.std())


# ### 4 Вставить 20 значений np.nan на случайные позиции в массиве

# In[49]:


rows, cols = matrix.shape
matrixWithNan = matrix.copy()
choice = np.random.choice(rows * cols, 20)
matrixWithNan.flat[choice] = np.nan
matrixWithNan


# ### 5 Найти позиции вставленных значений np.nan в 1-й колонке

# In[50]:


np.where(np.isnan(matrixWithNan[:,0]) == True)


# ### 6 Отфильтровать массив по условию значения в 3-й колонке > 1.5 и значения в 1-й колонке < 5.0

# In[51]:


matrix[(matrix[:,2] > 1.5) & (matrix[:,0] < 5.0)]


# ### 7 Заменить все значения np.nan на 0

# In[52]:


np.nan_to_num(matrixWithNan)


# ### 8 Посчитать все уникальные значения в массиве и вывести их вместе с подсчитанным количеством

# In[53]:


value, count = np.unique(matrix, return_counts=True)
print("Value: ", value)
print("Count: ", count)


# ### 9 Разбить массив по горизонтали на 2 массива

# In[54]:


array1, array2 = np.vsplit(matrix, 2)
print("array1 shape: ", array1.shape)
print("array2 shape", array2.shape)


# ### 10 Отсортировать оба получившихся массива по 1-й колонке: 1-й по возрастанию, 2-й по убыванию

# In[55]:


sorted1 = array1[array1[:,0].argsort()]
print("array1 sorted: ", sorted1[:10])
sorted2 = array2[array2[:,0].argsort()[::-1]]
print("array2 sorted: ", sorted2[:10])


# ### 11 Склеить оба массива обратно

# In[56]:


concatenated = np.concatenate((array1, array2))
print("concatenated array shape", concatenated.shape)


# ### 12 Найти наиболее часто повторяющееся значение в массиве

# In[57]:


value, count = np.unique(matrix, return_counts=True)
maxIndex = np.argmax(count)
print("most repeated value: ", value[maxIndex])


# ### 13 Написать функцию, которая бы умножала все значения в колонке, меньше среднего значения в этой колонке, на 2,
# и делила остальные значения на 4. Применить к 3-й колонке

# In[58]:


def modify_column(column):
    col = column.copy()
    mean = np.mean(col)
    col_temp = np.where(col < mean, col * 2, col)
    result = np.where(col_temp >= mean, col_temp / 4, col_temp)
    return result


print("Original matrix", matrix[:10])
matrix[:,2] = modify_column(matrix[:,2])
print("Function result: ", matrix[:10])

