#!/usr/bin/env python
# coding: utf-8

# <center>
# <img src="../../img/ods_stickers.jpg">
# ## Открытый курс по машинному обучению
# <center>
# Автор материала: Юрий Кашницкий, программист-исследователь Mail.Ru Group <br> 
# 
# Материал распространяется на условиях лицензии [Creative Commons CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). Можно использовать в любых целях (редактировать, поправлять и брать за основу), кроме коммерческих, но с обязательным упоминанием автора материала.

# # <center>Домашнее задание № 1 (демо).<br> Анализ данных по доходу населения UCI Adult</center>

# **В задании предлагается с помощью Pandas ответить на несколько вопросов по данным репозитория UCI [Adult](https://archive.ics.uci.edu/ml/datasets/Adult) (качать данные не надо – они уже есть в репозитории).**

# Уникальные значения признаков (больше информации по ссылке выше):
# - age: continuous.
# - workclass: Private, Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked.
# - fnlwgt: continuous.
# - education: Bachelors, Some-college, 11th, HS-grad, Prof-school, Assoc-acdm, Assoc-voc, 9th, 7th-8th, 12th, Masters, 1st-4th, 10th, Doctorate, 5th-6th, Preschool.
# - education-num: continuous.
# - marital-status: Married-civ-spouse, Divorced, Never-married, Separated, Widowed, Married-spouse-absent, Married-AF-spouse.
# - occupation: Tech-support, Craft-repair, Other-service, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners, Machine-op-inspct, Adm-clerical, Farming-fishing, Transport-moving, Priv-house-serv, Protective-serv, Armed-Forces.
# - relationship: Wife, Own-child, Husband, Not-in-family, Other-relative, Unmarried.
# - race: White, Asian-Pac-Islander, Amer-Indian-Eskimo, Other, Black.
# - sex: Female, Male.
# - capital-gain: continuous.
# - capital-loss: continuous.
# - hours-per-week: continuous.
# - native-country: United-States, Cambodia, England, Puerto-Rico, Canada, Germany, Outlying-US(Guam-USVI-etc), India, Japan, Greece, South, China, Cuba, Iran, Honduras, Philippines, Italy, Poland, Jamaica, Vietnam, Mexico, Portugal, Ireland, France, Dominican-Republic, Laos, Ecuador, Taiwan, Haiti, Columbia, Hungary, Guatemala, Nicaragua, Scotland, Thailand, Yugoslavia, El-Salvador, Trinadad&Tobago, Peru, Hong, Holand-Netherlands.   
# - salary: >50K,<=50K

# In[3]:


import pandas as pd


# In[5]:


data = pd.read_csv('/home/deadpool/PycharmProjects/geekhub-ds-2020/homework-3/adult.data.csv')
data.columns = data.columns.str.replace("[()-]", "_")
data.head()


# **1. Сколько мужчин и женщин (признак *sex*) представлено в этом наборе данных?**

# In[6]:


sex = data['sex']
unique = pd.unique(sex)
print(f"Unique sex counts:\n{sex.value_counts()}")


# **2. Каков средний возраст (признак *age*) женщин?**

# In[7]:


data[sex == "Female"]["age"].mean()


# **3. Какова доля граждан Германии (признак *native-country*)?**

# In[24]:


counts = data["native_country"].value_counts()
germany_perc = (100 * counts['Germany']/counts.sum())
print(f"German citizens percentage: {germany_perc.__round__(ndigits=3)}%")


# **4-5. Каковы средние значения и среднеквадратичные отклонения возраста тех, кто получает более 50K в год (признак *salary*) и тех, кто получает менее 50K в год? **

# In[35]:


gt_50k = data[data["salary"] == '>50K']["age"]
lte_50k = data[data["salary"] == '<=50K']["age"]
print(f">50K: mean: {gt_50k.mean()}, std: {gt_50k.std()}")
print(f"<=50K: mean: {lte_50k.mean()}, std: {lte_50k.std()}")


# **6. Правда ли, что люди, которые получают больше 50k, имеют как минимум высшее образование? (признак *education – Bachelors, Prof-school, Assoc-acdm, Assoc-voc, Masters* или *Doctorate*)**

# In[170]:


high_values = ["Bachelors", "Prof-school", "Assoc-acdm", "Assoc-voc", "Masters", "Doctorate"]
gt_50k_education = data[data["salary"] == '>50K']["education"]
not_high_education = gt_50k_education[~gt_50k_education.isin(high_values)]
if not_high_education.empty:
    print("Statement is TRUE")
else:
    print("Statement is NOT TRUE")
print(f"Education:\n{data['education'].value_counts()}")
print(f">50K and not high education:\n{not_high_education.value_counts()}")


# **7. Выведите статистику возраста для каждой расы (признак *race*) и каждого пола. Используйте *groupby* и *describe*. Найдите таким образом максимальный возраст мужчин расы *Amer-Indian-Eskimo*.**

# In[118]:


groups = data[["race","sex","age"]].groupby(by=["race","sex"], sort=True)["age"]
describe = groups.describe().unstack().reset_index()
eskimo_max_male_age = describe[describe["race"] == "Amer-Indian-Eskimo"]["max"]["Male"]
print(f"eskimo max male age: {eskimo_max_male_age}")
describe


# **8. Среди кого больше доля зарабатывающих много (>50K): среди женатых или холостых мужчин (признак *marital-status*)? Женатыми считаем тех, у кого *marital-status* начинается с *Married* (Married-civ-spouse, Married-spouse-absent или Married-AF-spouse), остальных считаем холостыми.**

# In[136]:


status = data["marital_status"].value_counts()
married = data[(data["marital_status"].str.startswith("Married")) & (data["salary"] == ">50K") & (data["sex"] == "Male")]
not_married = data[(~data["marital_status"].str.startswith("Married")) & (data["salary"] == ">50K") & (data["sex"] == "Male")]
if len(married) > len(not_married):
    print("Married men earn more")
elif len(married) > len(not_married):
    print("Not married men earn more")
else:
    print("Married and not married men earn the same")


# **9. Какое максимальное число часов человек работает в неделю (признак *hours-per-week*)? Сколько людей работают такое количество часов и каков среди них процент зарабатывающих много?**

# In[154]:


hours_per_week_max = data['hours_per_week'].max()
print(f"Max hours per week: { hours_per_week_max }")
hours_per_week_max_count = len(data[data['hours_per_week'] == hours_per_week_max])
print(f"Max hours per week count: { hours_per_week_max_count }")
hours_per_week_max_and_high_salary_count = len(data[(data['hours_per_week'] == hours_per_week_max) & (data["salary"] == ">50K")])
print(f"Max hours per week and high salary count: { hours_per_week_max_and_high_salary_count }")
hours_per_week_max_and_high_salary_rate = 100 * hours_per_week_max_and_high_salary_count / hours_per_week_max_count
print(f"Max hours per week and high salary rate: { round(hours_per_week_max_and_high_salary_rate, ndigits=2) }")


# **10. Посчитайте среднее время работы (*hours-per-week*) зарабатывающих мало и много (*salary*) для каждой страны (*native-country*).**

# In[169]:


groups = data[["hours_per_week","salary","native_country"]].groupby(by=["native_country","salary"], sort=True)["hours_per_week"]
describe = groups.describe().unstack()["mean"]
describe

